# Default target does nothing, intentionally
.PHONY: dummy
dummy:
	@echo hello

.PHONY: ssh-config
ssh-config:
	./bin/update-ssh-config > usr/share/kali-defaults/etc/ssh/ssh_config.d/kali-wide-compat.conf
